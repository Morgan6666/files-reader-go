# files-reader-go

# Сервис для обработки файлов содержится в device-reader-go
# Для запуска базы используйте комманду docker-compose up --build -d device
# Миграции содержаться в папке platform/migrations
# Для запуска rabbitmq используйте комманду docker-compose up --build -d rabbit1
# Создайте файл .env. Поместите в него слудующие значения
## TAGE_STATUS="dev"

# Server settings:
## SERVER_HOST="0.0.0.0"
## SERVER_PORT=5000
## SERVER_READ_TIMEOUT=60


# Directory settings
## SCAN_DIR='/home/morgan/biocad/test/'
## OUT_DIR='/home/morgan/biocad/output/result/'
## OUT_ERROR_DIR='/home/morgan/biocad/output/error/'
# Database settings:
## DB_TYPE="pgx"   # pgx or mysql
## DB_HOST="localhost"
## DB_PORT=5449
## DB_USER="morgan"
## DB_PASSWORD="test"
## DB_NAME="device"
## DB_SSL_MODE="disable"
## DB_MAX_CONNECTIONS=100
## DB_MAX_IDLE_CONNECTIONS=10
## DB_MAX_LIFETIME_CONNECTIONS=2

# Rabbit settings
## RABBIT_USER="rabbitmq"
## RABBIT_PASSWORD="rabbitmq"
## RABBIT_HOST="localhost"
## RABBIT_ROUTING_KEY="my_routing_key"
## RABBIT_CONSUMER_EXCHANGE_NAME="events"
## RABBIT_PUBLISHER_EXCHANGE_NAME="events"

# Для локального запуска используйте комманду go run main.go (нужно находиться в папке device-reader-go)

# По любым вопросам пишите на dbairamkulow@mail.ru