package models

type FilesData struct {
	UintGuid int    `tsv:"uint_guid"`
	Name     string `tsv:"name"`
	FileName string
}

type GetFileData struct {
	UnitGuid   int    `json:"unit_guid" db:"unit_guid"`
	FileName   string `json:"file_name" db:"file_name"`
	DeviceName string `json:"device_name" db:"device_name"`
}

type FileGuid struct {
	UnitGuid int `json:"unit_guid" db:"unit_guid"`
	Limit    int `json:"limit" db:"limit"`
}
type CheckFilesExist struct {
	FileName string `json:"file_name" db:"file_name"`
}

type FilesRowsCount struct {
	Count      int    `json:"count" db:"count"`
	DeviceName string `json:"device_name" db:"device_name"`
	UintGuid   int    `json:"uint_guid" db:"uint_guid"`
}

type GetFileID struct {
	FileID int `json:"id" db:"id"`
}
