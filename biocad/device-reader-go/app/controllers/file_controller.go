package controllers

import (
	"device-reader-go/app/models"
	"device-reader-go/pkg/rabbit"
	"device-reader-go/pkg/utils"
	"device-reader-go/platform/database"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"

	"github.com/google/logger"

	"github.com/dogenzaka/tsv"
	"github.com/gofiber/fiber/v2"
	"github.com/wagslane/go-rabbitmq"
)

func GetFileData(c *fiber.Ctx) error {
	fileData := &models.FileGuid{}
	if err := c.BodyParser(fileData); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"Success": false,
			"Message": err.Error(),
		})

	}
	db, err := database.OpenDBConnection()
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"Success": false,
			"Message": err.Error(),
		})
	}
	data, err := db.GetFiles(fileData)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"Success": false,
			"Message": err.Error(),
		})
	}
	return c.JSON(fiber.Map{
		"Success": true,
		"Message": "Успешно",
		"Content": data,
		"Code":    200,
	})
}

func FileProcessingCore(f string) error {

	db, err := database.OpenDBConnection()
	fileName := strings.Split(f, "/")[5]
	file, err := os.Open(f)
	data := models.FilesData{}
	res_data := []models.FilesData{}

	if err != nil {
		errLog, err := utils.AddErrorFile(err.Error())
		logger.Info("Error added:%s", errLog)
		if err != nil {
			logger.Info("Error create error file:%s", err)
		}

	}
	parser, _ := tsv.NewParser(file, &data)
	for {
		eof, err := parser.Next()
		if err != nil {
			errLog, err := utils.AddErrorFile(err.Error())
			logger.Info("Error added:%s", errLog)
			if err != nil {
				logger.Info("Error create error file:%s", err)
			}
		}
		if eof {
			break
		} else {
			data.FileName = fileName
			res_data = append(res_data, data)

		}

	}

	for _, k := range res_data {
		logger.Info("Add files rows:%v", f)
		check := db.CheckFilesExists(&models.CheckFilesExist{FileName: k.FileName})

		if len(check.FileName) == 0 {

			if err := db.AddFilesData(&k); err != nil {
				errLog, err := utils.AddErrorFile(err.Error())
				logger.Info("Error added:%s", errLog)
				if err != nil {
					logger.Info("Error create error file:%s", err)
				}

			}
			if err := db.AddRows(&k); err != nil {
				errLog, err := utils.AddErrorFile(err.Error())
				logger.Info("Error added:%s", errLog)
				if err != nil {
					logger.Info("Error create error file:%s", err)
				}

			}
			fileName := os.Getenv("OUT_DIR") + strconv.Itoa(k.UintGuid) + ".pdf"

			file, err := os.OpenFile(fileName, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0660)
			if err != nil {
				errLog, err := utils.AddErrorFile(err.Error())
				logger.Info("Error added:%s", errLog)
				if err != nil {
					logger.Info("Error create error file:%s", err)
				}
			}
			defer file.Close()
			fmt.Fprintf(file, "%v\n%s\n", k.UintGuid, k.Name)

		}

	}
	logger.Info("File is processing:%v", f)

	return nil
}

func Piblisher() error {
	db, err := database.OpenDBConnection()
	files, err := ioutil.ReadDir(os.Getenv("SCAN_DIR"))
	if err != nil {
		errLog, err := utils.AddErrorFile(err.Error())
		logger.Info("Error added:%s", errLog)
		if err != nil {
			errLog, err := utils.AddErrorFile(err.Error())
			logger.Info("Error added:%s", errLog)
			if err != nil {
				logger.Info("Error create error file:%s", err)
			}
		}

	}
	for _, f := range files {
		check := db.CheckFilesExists(&models.CheckFilesExist{FileName: f.Name()})
		if len(check.FileName) == 0 {
			logger.Info("Добавляем файлы")
			go rabbit.Publisher(os.Getenv("SCAN_DIR") + f.Name())
		}

	}
	logger.Info("Publisher done")
	return nil

}

func Consumer() {
	conn, err := rabbitmq.NewConn(
		fmt.Sprintf("amqp://%s:%s@%s", os.Getenv("RABBIT_USER"), os.Getenv("RABBIT_PASSWORD"), os.Getenv("RABBIT_HOST")),
		rabbitmq.WithConnectionOptionsLogging,
	)
	if err != nil {
		errLog, err := utils.AddErrorFile(err.Error())
		logger.Info("Error added:%s", errLog)
		if err != nil {
			logger.Info("Error create error file:%s", err)
		}

	}
	defer conn.Close()

	consumer, err := rabbitmq.NewConsumer(
		conn,
		func(d rabbitmq.Delivery) rabbitmq.Action {
			log.Printf("consumed: %v", string(d.Body))
			go FileProcessingCore(string(d.Body))
			return rabbitmq.Ack
		},
		"my_queue",
		rabbitmq.WithConsumerOptionsRoutingKey(fmt.Sprintf("%s", os.Getenv("RABBIT_ROUTING_KEY"))),
		rabbitmq.WithConsumerOptionsExchangeName(fmt.Sprintf("%s", os.Getenv("RABBIT_CONSUMER_EXCHANGE_NAME"))),
		rabbitmq.WithConsumerOptionsExchangeDeclare,
	)
	if err != nil {
		errLog, err := utils.AddErrorFile(err.Error())
		logger.Info("Error added:%s", errLog)
		if err != nil {
			logger.Info("Error create error file:%s", err)
		}
	}

	defer consumer.Close()

	// block main thread - wait for shutdown signal
	sigs := make(chan os.Signal, 1)
	done := make(chan bool, 1)

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		sig := <-sigs

		logger.Info("Signal is:%v", sig)

		done <- true

	}()

	logger.Info("awaiting signal")
	<-done

	logger.Info("stopping consumer")

}
