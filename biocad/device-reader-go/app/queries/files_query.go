package queries

import (
	"device-reader-go/app/models"
	"fmt"

	"github.com/jmoiron/sqlx"
)

type FilesQueries struct {
	*sqlx.DB
}

func (q *FilesQueries) AddFilesData(f *models.FilesData) error {
	query := `WITH f AS (
		INSERT INTO files_name (name) VALUES ($3) RETURNING id)
	
	INSERT INTO file_data(unit_guid, device_name, file_name_id)VALUES ($1, $2, (SELECT id FROM f))`
	err := q.QueryRow(query, f.UintGuid, f.Name, f.FileName)
	if err != nil {
		return err.Err()
	}
	return nil
}

func (q *FilesQueries) CheckFilesExists(f *models.CheckFilesExist) *models.CheckFilesExist {
	fmt.Println(f)
	data := &models.CheckFilesExist{}
	query := `SELECT name as file_name FROM files_name WHERE name = $1`
	err := q.Get(data, query, f.FileName)

	if err != nil {
		return data
	}
	return data
}

func (q *FilesQueries) CheckFilesRows(f *models.CheckFilesExist) (*models.FilesRowsCount, error) {
	data := &models.FilesRowsCount{}
	query := `SELECT COUNT(file_name_id),unit_quid,device_name FROM file_data WHERE file_name_id = (SELECT id FROM files_name WHERE name = $1)`
	err := q.Get(data, query, f.FileName)
	if err != nil {
		return data, err
	}
	return data, nil
}

func (q *FilesQueries) AddRows(f *models.FilesData) error {
	query := `INSERT INTO file_data(unit_guid, device_name, file_name_id)  VALUES($1,$2, (SELECT id FROM files_name WHERE name = $3));`
	err := q.QueryRow(query, f.UintGuid, f.Name, f.FileName)
	if err != nil {
		return err.Err()
	}
	return nil
}

func (q *FilesQueries) GetFiles(f *models.FileGuid) (*[]models.GetFileData, error) {
	data := &[]models.GetFileData{}
	query := `SELECT device_name as device_name,
				unit_guid,
				fn.name as file_name
			  FROM file_data
			  INNER JOIN files_name fn on fn.id = file_data.file_name_id
			 WHERE file_data.unit_guid = $1 LIMIT  $2`
	err := q.Select(data, query, f.UnitGuid, f.Limit)
	if err != nil {
		return data, err
	}
	return data, nil

}
