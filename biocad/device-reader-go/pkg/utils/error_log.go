package utils

import (
	"fmt"
	"os"

	"github.com/google/logger"
)

func AddErrorFile(error_value string) (string, error) {
	errName := os.Getenv("OUT_ERROR_DIR") + "error.pdf"
	errFile, err := os.OpenFile(errName, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0660)
	if err != nil {
		logger.Info(error_value)

	}
	defer errFile.Close()
	fmt.Fprintf(errFile, "%v\n", err.Error())
	return "Успешно", nil

}
