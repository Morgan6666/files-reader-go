package utils

const CODE_404 int = 404
const CODE_500 int = 500
const CODE_403 int = 403
const CODE_200 int = 200

const FOREBIDEN string = "Forebiden"
const NOT_FOUND string = "Not found"
const SUCCESS string = "Success"
const BAD_REQUEST string = "Bad request"
const INTERNAL_SERVER_ERROR = "Internal server error"
