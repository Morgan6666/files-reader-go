package utils

import (
	"device-reader-go/pkg/utils_models"

	"github.com/gofiber/fiber/v2"
)

func ResponseFormat(c *fiber.Ctx, data *utils_models.SuccessRes) error {
	return c.JSON(fiber.Map{
		"Success": data.Success,
		"Message": data.Message,
		"Content": data.Content,
		"Code":    data.Code,
	})
}
func NotFound() error {
	return ResponseFormat(&fiber.Ctx{}, &utils_models.SuccessRes{
		Success: true,
		Message: NOT_FOUND,
		Content: nil,
		Code:    CODE_404,
	})
}

func SuccessWithoutData() error {
	return ResponseFormat(&fiber.Ctx{}, &utils_models.SuccessRes{
		Success: true,
		Message: SUCCESS,
		Content: nil,
		Code:    CODE_200,
	})
}

func SucessWithData(content interface{}) error {
	return ResponseFormat(&fiber.Ctx{}, &utils_models.SuccessRes{
		Success: true,
		Message: SUCCESS,
		Content: content,
		Code:    CODE_200,
	})
}
