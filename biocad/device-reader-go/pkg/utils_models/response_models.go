package utils_models

type SuccessRes struct {
	Success bool        `json:"Success"`
	Message string      `json:"Message"`
	Content interface{} `json:"Content"`
	Code    int         `json:"Code"`
}
