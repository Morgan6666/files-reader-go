package routes

import (
	"github.com/gofiber/fiber/v2"
)

// PrivateRoutes func for describe group of private routes.
func PrivateRoutes(a *fiber.App) {
	// Create routes group.

	// Routes for POST method:
	// renew Access & Refresh tokens

	// Routes for PUT method:
	// delete one book by ID
}
