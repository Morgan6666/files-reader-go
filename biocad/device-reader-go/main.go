package main

import (
	"os"
	"time"

	"device-reader-go/app/controllers"
	"device-reader-go/pkg/configs"
	"device-reader-go/pkg/middleware"
	"device-reader-go/pkg/routes"
	"device-reader-go/pkg/utils"

	"github.com/gofiber/fiber/v2"
	"github.com/robfig/cron"

	_ "device-reader-go/docs" // load API Docs files (Swagger)

	_ "github.com/joho/godotenv/autoload" // load .env file automatically
)

// @title API
// @version 1.0
// @description This is an auto-generated API Docs.
// @termsOfService http://swagger.io/terms/
// @contact.name API Support
// @contact.email your@mail.com
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @BasePath /api
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func main() {
	// Define Fiber config.
	config := configs.FiberConfig()

	// Define a new Fiber app with config.
	app := fiber.New(config)

	// Middlewares.
	middleware.FiberMiddleware(app) // Register Fiber's middleware for app.

	// Routes.
	routes.SwaggerRoute(app)  // Register a route for API Docs (Swagger).
	routes.PublicRoutes(app)  // Register a public routes for app.
	routes.PrivateRoutes(app) // Register a private routes for app.
	routes.NotFoundRoute(app)
	if !fiber.IsChild() {
		go Sheduler()

	}
	// Register route for 404 Error.

	// Start server (with or without graceful shutdown).
	if os.Getenv("STAGE_STATUS") == "dev" {

		utils.StartServer(app)

	} else {
		utils.StartServerWithGracefulShutdown(app)
	}
}

func Sheduler() {
	c := cron.New()
	c.AddFunc("*/1 * * * *", (func() {
		controllers.Piblisher()

		controllers.Consumer()
	}))

	c.Start()
	time.Sleep(20 * time.Second)
}
